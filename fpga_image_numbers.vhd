-- -------------------------------------------------------------------------------
--
--       Copyright  (c)  2016  ;  Knorr-Bremse Rail Systems (UK) Ltd
--
--        This program is protected by copyright and the information
--         contained therein is confidential.  The program may not be
--          copied and the information may not be used or disclosed
--          except with the written permission of the proprietors
--                        Knorr-Bremse Rail Systems (UK) Ltd.
--
-- -------------------------------------------------------------------------------
--
--             Project Name  :  VD fpga
--                FPGA Name  :  Altera 10M08SAU169I7G
--              Object Name  :
--                File Name  :  ex_fpga_image_numbers.vhd
--            Function List  :  None
--                   Author  :  Mark Bonning 
--                     Date  :  23/08/2019
--                                                                          
-- -------------------------------------------------------------------------------
-- Description
-- Holds the Ex fpga image part number and issue number to connect to the mux. 
-- -------------------------------------------------------------------------------
-- History
-- Issue 0.01: Mark Bonning 
-- -------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use IEEE.numeric_std.all;


entity fpga_image_numbers is
    port (

		reg_o			: out std_logic_vector(15 downto 0)
    );
end entity fpga_image_numbers;

architecture imp_fpga_image_numbers of fpga_image_numbers is

	
constant Part_Number 	: std_logic_vector(7 downto 0) := x"03";
constant Issue_Number 	: std_logic_vector(7 downto 0) := x"04";

begin


-- logic

reg_o(15 downto 8) 	<= Part_Number;
reg_o(7 downto 0) 	<= Issue_Number;


-------------------------------------------------------------------------------------------------------------------------------------------------------------------

end architecture imp_fpga_image_numbers;

