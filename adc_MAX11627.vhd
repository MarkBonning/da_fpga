-- -------------------------------------------------------------------------------
--
--       Copyright  (c)  2016  ;  Knorr-Bremse Rail Systems (UK) Ltd
--
--        This program is protected by copyright and the information
--         contained therein is confidential.  The program may not be
--          copied and the information may not be used or disclosed
--          except with the written permission of the proprietors
--                        Knorr-Bremse Rail Systems (UK) Ltd.
--
-- -------------------------------------------------------------------------------
--
--             Project Name  :  EX FPGA
--                FPGA Name  :  Altera 10M08SAU169I7G
--              Object Name  :
--                File Name  :  adc_MAX11627-MB.vhd
--            Function List  :  None
--                   Author  :  Mark Bonning 
--                     Date  :  07/01/2019 
--                                                                          
-- -------------------------------------------------------------------------------
-- Description
-- Controls an SPI master to drive the MAX11627 ADC
-- To keep all MAX11627 modules the same, read the highest numner of used channels: 4
-- -------------------------------------------------------------------------------
-- History
-- Issue 0.01: Mark Bonning 
-- -------------------------------------------------------------------------------
library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
--USE ieee.std_logic_arith.all;
use IEEE.std_logic_unsigned.all;

entity adc_max11627 is
    port 	(

      clock20mhz_i        :  in std_logic;
      reset_ni            :  in std_logic;
        
     	enable_o				: out std_logic;
      cpol_o				: out std_logic;
		cpha_o				: out std_logic;
		cont_o				: out std_logic;
		tx_data_o			: out std_logic_vector(7 downto 0);
		busy_i				: in std_logic;
		adc_eoc_ni			: in std_logic;
		dataWrite_o			: out std_logic;
		rx_data_i			: in std_logic_vector(7 downto 0);

		adc_data_1_o		: out std_logic_vector(15 downto 0);
		adc_data_2_o		: out std_logic_vector(15 downto 0);
		adc_data_3_o		: out std_logic_vector(15 downto 0);
		adc_data_4_o		: out std_logic_vector(15 downto 0);

      pulse_1ms_i      	:  in std_logic
		
		
        );
end;

architecture RTL of adc_max11627 is


type states is ( START, SETUP, SETUP_WAIT, SETUP_LOOP,
					  START_CONVERSION, CONVERSION_WAIT, CONVERSION_DONE, READ_WAIT, READ_READY, READ_DONE );
signal state_s   	: states;

signal adc_data_s	: std_logic_vector(63 downto 0);
signal dataWrite_s : std_logic;
signal adc_timeout_s						: std_logic;


-- conversion register value to start 4 channels of reads
constant convertion_register_val_c	: std_logic_vector(7 downto 0) := x"98";

-- adc timeout value (1000ms)
constant timeeout_val_c					: integer := 1000;

-- number of setup bytes to write
constant	setup_byte_count_c			: integer := 2;

-- number of data bytes to read (4 channels x 16 bits)
constant	read_byte_count_c				: integer := 8;



begin

	-- drive constant pins
	cpol_o <= '1';
	cpha_o <= '1';
	cont_o <= '0';

	-- map signals to ports
	dataWrite_o <= dataWrite_s;
	
    -- 
    adc : 	process ( reset_ni, clock20mhz_i ) is

		variable count : integer range 0 to 10:= 0;
		variable timeout_count 	: integer range 0 to 1000 	:= 0;
		
		
		-- variables for setup lookup table
		 type MEM is array(0 to 1) of std_logic_vector(7 downto 0);
		 variable adc_setup : MEM;
	
    
    
    begin
	 
	 -- register setup values;
	 adc_setup := ( x"68", x"20" );
	 
	 
	 
        if ( reset_ni = '0' ) then
				enable_o <= '0';
            count := 0;
				adc_data_s <= (others => '0');
				dataWrite_s <= '0';
				
				adc_timeout_s <= '0';

				timeout_count := 0;
				
				
            state_s <= START;

        elsif Rising_Edge ( clock20mhz_i ) then



				-- create 1 second timeout in case adc's lock up or lose power
				-- force a restart in FSM
				if timeout_count = timeeout_val_c then
				
					adc_timeout_s <= '1';
					
				-- increment count on 1ms pulse
				elsif pulse_1ms_i = '1' then
				
					adc_timeout_s <= '0';
					timeout_count := timeout_count + 1;
					
				end if;
        

-- state machine to run the transfer to the SPI module

----------------------------------------------------
-- Send setup data value - loop through each register
----------------------------------------------------

					case state_s is
						when START =>
						
							-- clear timeout count
							timeout_count := 0;
						
							if busy_i = '0' and pulse_1ms_i = '1' then
								state_s <= SETUP;
							else
								state_s <= START;
							end if;
						when SETUP =>
							dataWrite_s <= '0';
							enable_o <= '1';
							tx_data_o <= adc_setup(count);
							if busy_i = '0' then
								state_s <= SETUP;
							else
								state_s <= SETUP_WAIT;
							end if;
						when SETUP_WAIT =>
							enable_o <= '0';
							if busy_i = '0' then
								count := count + 1;
								state_s <= SETUP_LOOP;
							else
								state_s <= SETUP_WAIT;
							end if;
						when SETUP_LOOP =>
							enable_o <= '0';
							if count = setup_byte_count_c then
								count := 0;
								state_s <= START_CONVERSION;
							else
								state_s <= SETUP;
							end if;


----------------------------------------------------
-- Perform conversion
----------------------------------------------------

						when START_CONVERSION =>
							enable_o <= '1';
							dataWrite_s <= '0';
							-- scan channels 0 through to 3
							tx_data_o <= convertion_register_val_c;	
							if busy_i = '0' then
								state_s <= START_CONVERSION;
							else
								state_s <= CONVERSION_WAIT;
							end if;
						when CONVERSION_WAIT =>
							enable_o <= '0';
							if adc_eoc_ni = '0' then

								state_s <= CONVERSION_DONE;
								
							-- check if a timeout has occured, conversion taken too long
							elsif adc_timeout_s = '1' then
								
								-- something failed, go back to the start
								state_s <= START;
								
							else
								state_s <= CONVERSION_WAIT;
							end if;
							
						when CONVERSION_DONE =>
							enable_o <= '1';
							tx_data_o <= x"00";
							if busy_i = '0' then
								state_s <= CONVERSION_DONE;
							else
								state_s <= READ_WAIT;
							end if;						

						when READ_WAIT =>
							enable_o <= '0';
							if busy_i = '1' then
								state_s <= READ_WAIT;
							else
								count := count + 1;
								state_s <= READ_READY;
							end if;						
						when READ_READY =>
							adc_data_s <= adc_data_s(55 downto 0) & rx_data_i;  
							if(count = read_byte_count_c) then
								count := 0;
								state_s <= READ_DONE;
							else
								state_s <= CONVERSION_DONE;
							end if;
						when READ_DONE =>
								dataWrite_s <= '1';
								
	adc_data_1_o <= adc_data_s(63 downto 48);
	adc_data_2_o <= adc_data_s(47 downto 32);
	adc_data_3_o <= adc_data_s(31 downto 16);
								
	adc_data_4_o <= adc_data_s(15 downto 0);
								state_s <= START;
							
						-- something went wrong, re-setup adc
		        		when others =>
							state_s <= START;
					end case;		          

					
        end if;
    end process;


end architecture RTL;