-- -------------------------------------------------------------------------------
--
--       Copyright  (c)  2016  ;  Knorr-Bremse Rail Systems (UK) Ltd
--
--        This program is protected by copyright and the information
--         contained therein is confidential.  The program may not be
--          copied and the information may not be used or disclosed
--          except with the written permission of the proprietors
--                        Knorr-Bremse Rail Systems (UK) Ltd.
--
-- -------------------------------------------------------------------------------
--
--             Project Name  :  EX fpga
--                FPGA Name  :  Altera 10M08SAU169I7G
--              Object Name  :
--                File Name  :  output4_enable.vhd
--            Function List  :  None
--                   Author  :  Mark Bonning 
--                     Date  :  23/01/2019
--                                                                          
-- -------------------------------------------------------------------------------
-- Description
-- Monitors OP_ENABLE_FPGA and sets ISO4_OP_ENABLE appropriately 
-- -------------------------------------------------------------------------------
-- History
-- Issue 0.01: Mark Bonning 
-- -------------------------------------------------------------------------------
library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;


entity iso_output_enable is
    port 	(

			clock20mhz_i           :  in std_logic;
			reset_ni               :  in std_logic;
			output_enable_i				: in std_logic;
			iso_output_enable_o  		: out std_logic
        );
end;

architecture RTL of iso_output_enable is

	signal clock1ms_s		: std_logic;
	signal clock1ms_counter_s	: unsigned(14 downto 0);
	constant clock1ms_limit		: unsigned(14 downto 0) := "010011100010000";
	

	signal   counter_output_s 		  : unsigned(9 downto 0) ;
	constant output_limit_s		  : unsigned(9 downto 0) := "1111101000";	-- 1000ms

	
begin


    --counter to divide the 20mhz down to 1khz - make the timer more manageable
    clock_div : 	process ( reset_ni, clock20mhz_i) is
    begin
        if ( reset_ni = '0' ) then
            clock1ms_counter_s <= ( others => '0' );
				clock1ms_s <= '0';
        elsif Rising_Edge ( clock20mhz_i ) then
				if clock1ms_counter_s = clock1ms_limit then
					clock1ms_counter_s <= ( others => '0' ) ;
					clock1ms_s <= not (clock1ms_s);
				else
					clock1ms_counter_s <= clock1ms_counter_s + 1;
				end if;
        end if;
    end process;



    --counter to set the watchdog refresh signal
    output4 : 	process ( reset_ni, clock1ms_s ) is
    begin
        if ( reset_ni = '0' ) then
            counter_output_s <= ( others => '0' );
				iso_output_enable_o <= '0';
        elsif Rising_Edge ( clock1ms_s ) then

			-- if input zero, then zero the count else, increment the count
			if( output_enable_i = '0') then
				counter_output_s <= ( others => '0' );
				iso_output_enable_o <= '0';				
			elsif( counter_output_s = output_limit_s) then
				iso_output_enable_o <= '1';
			else
				iso_output_enable_o <= '0';				
				counter_output_s <= counter_output_s + 1;
			end if;

        end if;
    end process;



end architecture RTL;