-- -------------------------------------------------------------------------------
--                                                                          
--       Copyright  (c)  2018  ;  Knorr-Bremse Rail Systems (UK) Ltd        
--                                                                            
--        This program is protected by copyright and the information         
--         contained therein is confidential.  The program may not be         
--          copied and the information may not be used or disclosed           
--          except with the written permission of the proprietors            
--                        Knorr-Bremse Rail Systems (UK) Ltd.                 
--                                                                           
-- -------------------------------------------------------------------------------
--                                                                           
--             Project Name  :  EX FPGA
--                FPGA Name  :  Altera 10M08SAU169I7G                          
--                File Name  :  read_relay.vhd                                  
--                   Author  :  Mark Bonning 
--                     Date  :  07/01/2019 
--                                                                          
-- -------------------------------------------------------------------------------
-- Description
-- Drives the read_relay_nc_contact_fpga_i and read_relay_no_contact_i signals
-- -------------------------------------------------------------------------------
-- History
-- Issue 0.01: Mark Bonning - initial design
--             
-- -------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity read_relay is
	port 	(

		nc_input_2_i				: in std_logic;
		no_input_2_i				: in std_logic;
		nc_input_3_i				: in std_logic;
		no_input_3_i				: in std_logic;
		nc_input_4_i				: in std_logic;
		no_input_4_i				: in std_logic;
		nc_input_5_i				: in std_logic;
		no_input_5_i				: in std_logic;
		

		read_relay_nc_contact_fpga_o	: out std_logic;
		read_relay_no_contact_fpga_o	: out std_logic
		
		);
end; 

architecture RTL of read_relay is

	-- signals


	
	
begin
		
	-- read_relay_nc_contact_fpga_o are high whenever any relay contact is high else follows read_relay_nc_contact_i

	read_relay_nc_contact_fpga_o <= nc_input_2_i or nc_input_3_i or nc_input_4_i or nc_input_5_i;
	
	read_relay_no_contact_fpga_o <= no_input_2_i or no_input_3_i or no_input_4_i or no_input_5_i;
	
	
	
end architecture RTL;