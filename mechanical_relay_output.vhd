-- -------------------------------------------------------------------------------
--                                                                          
--       Copyright  (c)  2018  ;  Knorr-Bremse Rail Systems (UK) Ltd        
--                                                                            
--        This program is protected by copyright and the information         
--         contained therein is confidential.  The program may not be         
--          copied and the information may not be used or disclosed           
--          except with the written permission of the proprietors            
--                        Knorr-Bremse Rail Systems (UK) Ltd.                 
--                                                                           
-- -------------------------------------------------------------------------------
--                                                                           
--             Project Name  :  EX FPGA
--                FPGA Name  :  Altera 10M08SAU169I7G                          
--                File Name  :  mechanical_relay_output.vhd                                  
--                   Author  :  Mark Bonning 
--                     Date  :  07/01/2019 
--                                                                          
-- -------------------------------------------------------------------------------
-- Description
-- Mechanical relay driver
-- -------------------------------------------------------------------------------
-- History
-- Issue 0.01: Mark Bonning - initial design
--             
-- -------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity mechanical_relay_output is
	port 	(
	
		clock20m_i							: in std_logic;		
		reset_ni								: in std_logic;		
		
		relay_in_i							: in std_logic;
		relay_energise_o					: out std_logic;
		
		read_relay_nc_contact_i			: in std_logic;
		read_relay_no_contact_i			: in std_logic;

		read_relay_nc_contact_fpga_o	: out std_logic;
		read_relay_no_contact_fpga_o	: out std_logic;
		
		pulse_1ms							: in std_logic
		
		);
end; 

architecture RTL of mechanical_relay_output is

	-- signals
	signal rising_running_s			: std_logic;
	signal falling_running_s		: std_logic;


	-- rising edge detect signals
	signal s0_s							: std_logic;									-- edge detect signal 0
	signal s1_s							: std_logic;									-- edge detect signal 1
	signal rising_detect_s			: std_logic;								-- single clock pulse on rising edge
	signal falling_detect_s			: std_logic;								-- single clock pulse on falling edge
	
	constant delay_50ms 				: integer := 50;
	
begin
		
	--map registers to ports
	
-- single clock pulse on rising edge of relay_in_i
rising_detect_s <= not s1_s and s0_s;
falling_detect_s <= not s0_s and s1_s;

	
-------------------------------------------------------------------------------------------------------------------------------------------------------------------
-- Rising / falling edge logic
-------------------------------------------------------------------------------------------------------------------------------------------------------------------
	rising_detect : process (reset_ni, clock20m_i)
	begin
		if reset_ni = '0' then
			s0_s <= '0';
			s1_s <= '0';
		elsif rising_edge (clock20m_i) then
			s0_s <= relay_in_i;
			s1_s <= s0_s;
		end if;
	end process;	
	
	

-------------------------------------------------------------------------------------------------------------------------------------------------------------------
-- Rising edge logic
-------------------------------------------------------------------------------------------------------------------------------------------------------------------

	-- NO Relay Output is logic 1 when the 50ms counter is running
	read_relay_no_contact_fpga_o <= '1' when rising_running_s = '1' else read_relay_no_contact_i;

	no_relay_control : process (reset_ni, clock20m_i)

	variable rising_count	      : integer range 0 to 50 := 0;
	
	begin
		-- clear registers when reset is low
		if reset_ni = '0' then
			rising_count := 0;
			relay_energise_o <= '0';
			rising_running_s <= '0';

		-- clock data in on rising edge of clock
		elsif rising_edge(clock20m_i) then	 

			-- if rising edge of relay_in_i, zero the count and set outputs
			if rising_detect_s = '1' then
				rising_count := 0;
				
				rising_running_s <= '1';
				relay_energise_o <=  '1';
				
			-- if the count gets to 1 million = 50ms then stop count 
			elsif rising_count = delay_50ms then

				rising_running_s <= '0';
				relay_energise_o <=  '0';

				
			-- else, increment the count
			elsif( rising_running_s = '1') then

				if pulse_1ms = '1' then
					rising_count := rising_count + 1;
				end if;
				
			end if;
			
			
	 	end if;
	end process;
	
-------------------------------------------------------------------------------------------------------------------------------------------------------------------
-- Falling edge logic
-------------------------------------------------------------------------------------------------------------------------------------------------------------------

	-- NC Relay Output is logic 1 when the 50ms counter is running
	read_relay_nc_contact_fpga_o <= '1' when falling_running_s = '1' else read_relay_nc_contact_i;

	nc_relay_control : process (reset_ni, clock20m_i)

	variable falling_count	      : integer range 0 to 50 := 0;
	
	begin
		-- clear registers when reset is low
		if reset_ni = '0' then
			falling_count := 0;
			falling_running_s <= '0';

		-- clock data in on rising edge of clock
		elsif rising_edge(clock20m_i) then	 

			-- if falling edge of relay_in_i, zero the count and set outputs
			if falling_detect_s = '1' then

				falling_count := 0;
				falling_running_s <= '1';
				
			-- if the count gets to 1 million = 50ms then stop count 
			elsif falling_count = delay_50ms then

				falling_running_s <= '0';
				
			-- else, increment the count
			elsif falling_running_s = '1' then

				if pulse_1ms = '1' then
					falling_count := falling_count + 1;
				end if;
				
			end if;
			
			
	 	end if;
	end process;
	
	
end architecture RTL;